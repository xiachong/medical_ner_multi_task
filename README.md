# NER

## Base model

./model/charbigru

./model/charbilstm

./model/transbilstm

./model/charcnn

./model/crf

./model/wordsequence

## Other model

./model/wordrep:word-level + character-level + translation level

./model/wordsequence:将联合特征输入nn进行最终表示学习

./model/seqmodel

## utils

./utils/alphabet

./utils/data

./utils/functions


## main

main.py

notice:
1. 数据最后一行空行
2. 替换\n\n\n为\n\n
3. alphabet size

