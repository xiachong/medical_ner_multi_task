# -*- coding: utf-8 -*-

import codecs
import random
import sys
import numpy as np
import time
import torch
from torch import autograd

from alphabet import Alphabet
from zhon.hanzi import punctuation
from string import punctuation as engpunc

from utils.metric import get_ner_fmeasure


def normalize_word(word):
    new_word = ""
    for char in word:
        if char.isdigit():
            new_word += '0'
        else:
            new_word += char
    return new_word


def read_instance(input_file, word_alphabet, char_alphabet, feature_alphabets, label_alphabet, number_normalized,
                  max_sent_length, translation_id_format, char_padding_size=-1, char_padding_symbol='</pad>', ):
    feature_num = len(feature_alphabets)
    in_lines = open(input_file, 'r').readlines()
    instence_texts = []
    instence_Ids = []
    words = []
    features = []
    chars = []
    labels = []
    word_Ids = []
    feature_Ids = []
    char_Ids = []
    label_Ids = []
    translation_Ids = []
    for line in in_lines:
        if len(line.strip().split()) > 1:
            pairs = line.strip().split()
            word = pairs[0].decode('utf-8')
            if number_normalized:
                word = normalize_word(word)
            label = pairs[-1]
            words.append(word)
            labels.append(label)
            word_Ids.append(word_alphabet.get_index(word))
            label_Ids.append(label_alphabet.get_index(label))
            ## get features
            feat_list = []
            feat_Id = []
            for idx in range(feature_num):
                feat_idx = pairs[idx + 1].split(']', 1)[-1]
                feat_list.append(feat_idx)
                feat_Id.append(feature_alphabets[idx].get_index(feat_idx))
            features.append(feat_list)
            feature_Ids.append(feat_Id)
            ## get char
            char_list = []
            char_Id = []
            for char in word:
                char_list.append(char)
            if char_padding_size > 0:
                char_number = len(char_list)
                if char_number < char_padding_size:
                    char_list = char_list + [char_padding_symbol] * (char_padding_size - char_number)
                assert (len(char_list) == char_padding_size)
            else:
                ### not padding
                pass
            for char in char_list:
                char_Id.append(char_alphabet.get_index(char))
            chars.append(char_list)
            char_Ids.append(char_Id)
        else:
            if (max_sent_length < 0) or (len(words) < max_sent_length):
                if len(word_Ids) > 0:
                    instence_texts.append([words, features, chars, labels])
                    if len(translation_id_format) > 0:
                        for word_id in word_Ids:
                            translation_Ids.append(translation_id_format[word_id])
                    instence_Ids.append([word_Ids, feature_Ids, char_Ids, translation_Ids, label_Ids])

            words = []
            features = []
            chars = []
            labels = []
            word_Ids = []
            feature_Ids = []
            char_Ids = []
            label_Ids = []
            translation_Ids = []
    return instence_texts, instence_Ids


def build_pretrain_embedding(embedding_path, word_alphabet, embedd_dim=100, norm=True):
    embedd_dict = dict()
    if embedding_path != None:
        embedd_dict, embedd_dim = load_pretrain_emb(embedding_path)
    alphabet_size = word_alphabet.size()
    scale = np.sqrt(3.0 / embedd_dim)
    pretrain_emb = np.empty([word_alphabet.size(), embedd_dim])
    perfect_match = 0
    case_match = 0
    not_match = 0
    delete_punc_match = 0
    for word, index in word_alphabet.iteritems():
        if word in embedd_dict:
            if norm:
                pretrain_emb[index, :] = norm2one(embedd_dict[word])
            else:
                pretrain_emb[index, :] = embedd_dict[word]
            perfect_match += 1
        elif word.lower() in embedd_dict:
            if norm:
                pretrain_emb[index, :] = norm2one(embedd_dict[word.lower()])
            else:
                pretrain_emb[index, :] = embedd_dict[word.lower()]
            case_match += 1
        elif (word[-1] in punctuation or word[-1] in engpunc) and word[0:-1] in embedd_dict:
            if norm:
                pretrain_emb[index, :] = norm2one(embedd_dict[word[0:-1]])
            else:
                pretrain_emb[index, :] = embedd_dict[word[0:-1]]
            delete_punc_match += 1
        else:
            pretrain_emb[index, :] = np.random.uniform(-scale, scale, [1, embedd_dim])
            not_match += 1
    pretrained_size = len(embedd_dict)
    print(
            "Embedding:\n     pretrain word:%s, prefect match:%s, case_match:%s,delete_punc_match:%s, oov:%s, oov%%:%s" % (
        pretrained_size, perfect_match, case_match, delete_punc_match, not_match, (not_match + 0.) / alphabet_size))
    return pretrain_emb, embedd_dim


def build_chi_pretrain_embedding(embedding_path, word_alphabet, embedd_dim=100, norm=True):
    embedd_dict = dict()
    if embedding_path != None:
        # key为utf-8
        embedd_dict, embedd_dim = load_pretrain_emb(embedding_path)
    alphabet_size = word_alphabet.size()
    scale = np.sqrt(3.0 / embedd_dim)
    pretrain_emb = np.empty([word_alphabet.size(), embedd_dim])
    perfect_match = 0
    case_match = 0
    not_match = 0
    for word, index in word_alphabet.iteritems():
        if word in embedd_dict:
            # print "perfect match:{}".format(word)
            if norm:
                pretrain_emb[index, :] = norm2one(embedd_dict[word])
            else:
                pretrain_emb[index, :] = embedd_dict[word]
            perfect_match += 1
        elif word.lower() in embedd_dict:
            if norm:
                pretrain_emb[index, :] = norm2one(embedd_dict[word.lower()])
            else:
                pretrain_emb[index, :] = embedd_dict[word.lower()]
            case_match += 1
        else:
            pretrain_emb[index, :] = np.random.uniform(-scale, scale, [1, embedd_dim])
            not_match += 1
    pretrained_size = len(embedd_dict)
    print("Embedding:\n     pretrain word:%s, prefect match:%s, case_match:%s, oov:%s, oov%%:%s" % (
        pretrained_size, perfect_match, case_match, not_match, (not_match + 0.) / alphabet_size))
    return pretrain_emb, embedd_dim


def norm2one(vec):
    root_sum_square = np.sqrt(np.sum(np.square(vec)))
    return vec / root_sum_square


def load_pretrain_emb(embedding_path):
    embedd_dim = -1
    embedd_dict = dict()
    with open(embedding_path, 'r') as file:
        for line in file:
            line = line.strip()
            if len(line) == 0:
                continue
            tokens = line.split()
            if embedd_dim < 0:
                embedd_dim = len(tokens) - 1
                print "embedd_dim:{}".format(embedd_dim)

            else:
                assert (embedd_dim + 1 == len(tokens))
            embedd = np.empty([1, embedd_dim])
            embedd[:] = tokens[1:]
            embedd_dict[tokens[0].decode('utf-8')] = embedd

    return embedd_dict, embedd_dim


def get_instance(batch_id, data, status):
    if status == "train":
        ids = data.train_Ids
    elif status == "dev":
        ids = data.dev_Ids
    elif status == 'test':
        ids = data.test_Ids
    total_batch = len(ids) // data.HP_batch_size + 1
    if batch_id >= total_batch:
        if status == "train":
            random.shuffle(data.train_Ids)
        elif status == "dev":
            random.shuffle(data.dev_Ids)
        elif status == 'test':
            random.shuffle(data.test_Ids)
        batch_id = 0
    start = batch_id * data.HP_batch_size
    end = (batch_id + 1) * data.HP_batch_size
    if end > len(ids):
        end = len(ids)
    instance = ids[start:end]
    batch_id = batch_id + 1
    return instance, batch_id


def batchify_with_label(input_batch_list, gpu, volatile_flag=False):
    """
        input: list of words, chars and labels, various length. [[words,chars, labels],[words,chars,labels],...]
            words: word ids for one sentence. (batch_size, sent_len)
            chars: char ids for on sentences, various length. (batch_size, sent_len, each_word_length)
        output:
            zero padding for word and char, with their batch length
            word_seq_tensor: (batch_size, max_sent_len) Variable
            word_seq_lengths: (batch_size,1) Tensor
            char_seq_tensor: (batch_size*max_sent_len, max_word_len) Variable
            char_seq_lengths: (batch_size*max_sent_len,1) Tensor
            char_seq_recover: (batch_size*max_sent_len,1)  recover char sequence order
            label_seq_tensor: (batch_size, max_sent_len)
            mask: (batch_size, max_sent_len)
    """
    batch_size = len(input_batch_list)
    # print batch_size
    # print "input_batch_list:{}".format(input_batch_list)
    words = [sent[0] for sent in input_batch_list]
    features = [np.asarray(sent[1]) for sent in input_batch_list]
    feature_num = len(features[0][0])
    # feature_num = 0
    chars = [sent[2] for sent in input_batch_list]
    trans = [sent[3] for sent in input_batch_list]
    labels = [sent[4] for sent in input_batch_list]
    #
    # print "words:{}".format(words)
    # print "chars:{}".format(chars)
    # print"trans:{}".format(trans)
    # print "labels:{}".format(labels)
    # exit(0)

    word_seq_lengths = torch.LongTensor(map(len, words))
    # print "word_seq_lengths:{}".format(word_seq_lengths)
    max_seq_len = word_seq_lengths.max()
    # print "max_seq_len:{}".format(max_seq_len)
    word_seq_tensor = autograd.Variable(torch.zeros((batch_size, max_seq_len)), volatile=volatile_flag).long()
    label_seq_tensor = autograd.Variable(torch.zeros((batch_size, max_seq_len)), volatile=volatile_flag).long()
    feature_seq_tensors = []
    for idx in range(feature_num):
        feature_seq_tensors.append(
            autograd.Variable(torch.zeros((batch_size, max_seq_len)), volatile=volatile_flag).long())
    mask = autograd.Variable(torch.zeros((batch_size, max_seq_len)), volatile=volatile_flag).byte()
    for idx, (seq, label, seqlen) in enumerate(zip(words, labels, word_seq_lengths)):
        word_seq_tensor[idx, :seqlen] = torch.LongTensor(seq)
        label_seq_tensor[idx, :seqlen] = torch.LongTensor(label)
        mask[idx, :seqlen] = torch.Tensor([1] * seqlen)
        for idy in range(feature_num):
            feature_seq_tensors[idy][idx, :seqlen] = torch.LongTensor(features[idx][:, idy])

    # print "word_seq_tensor befor reordering:{}".format(word_seq_tensor)
    # print "label_seq_tensor befor reordering:{}".format(label_seq_tensor)

    word_seq_lengths, word_perm_idx = word_seq_lengths.sort(0, descending=True)
    # print "word_seq_lengths after func sort:{}".format(word_seq_lengths)
    # print "word_perm_idx after func sort:{}".format(word_perm_idx)
    word_seq_tensor = word_seq_tensor[word_perm_idx]
    # print "word_seq_tensor reorder:{}".format(word_seq_tensor)
    for idx in range(feature_num):
        feature_seq_tensors[idx] = feature_seq_tensors[idx][word_perm_idx]

    label_seq_tensor = label_seq_tensor[word_perm_idx]
    mask = mask[word_perm_idx]
    # print "label_seq_tensor:{}".format(label_seq_tensor)
    # print "mask:{}".format(mask)

    ### deal with char
    # pad_chars (batch_size, max_seq_len)
    pad_chars = [chars[idx] + [[0]] * (max_seq_len - len(chars[idx])) for idx in range(len(chars))]
    # print "pad_chars:{}".format(pad_chars)
    length_list = [map(len, pad_char) for pad_char in pad_chars]
    # print "length_list:{}".format(length_list)
    max_word_len = max(map(max, length_list))
    # print "max_word_len:{}".format(max_word_len)
    char_seq_tensor = autograd.Variable(torch.zeros((batch_size, max_seq_len, max_word_len)),
                                        volatile=volatile_flag).long()
    char_seq_lengths = torch.LongTensor(length_list)
    # print "char_seq_lengths:{}".format(char_seq_lengths)
    for idx, (seq, seqlen) in enumerate(zip(pad_chars, char_seq_lengths)):
        for idy, (word, wordlen) in enumerate(zip(seq, seqlen)):
            # print len(word), wordlen
            char_seq_tensor[idx, idy, :wordlen] = torch.LongTensor(word)

    # print "char_seq_tensor:{}".format(char_seq_tensor)
    char_seq_tensor = char_seq_tensor[word_perm_idx].view(batch_size * max_seq_len, -1)
    # print "char_seq_tensor after reorder：{}".format(char_seq_tensor)
    char_seq_lengths = char_seq_lengths[word_perm_idx].view(batch_size * max_seq_len, )
    # print "char_seq_lengths after reorder：{}".format(char_seq_lengths)
    char_seq_lengths, char_perm_idx = char_seq_lengths.sort(0, descending=True)
    # print "char_seq_lengths:{}".format(char_seq_lengths)
    # print "char_perm_idx:{}".format(char_perm_idx)
    char_seq_tensor = char_seq_tensor[char_perm_idx]
    # print "char_seq_tensor:{}".format(char_seq_tensor)
    _, char_seq_recover = char_perm_idx.sort(0, descending=False)
    # print "char_seq_recover:{}".format(char_seq_recover)
    _, word_seq_recover = word_perm_idx.sort(0, descending=False)
    # print "word_seq_recover:{}".format(word_seq_recover)

    ### deal with trans
    pad_trans = [trans[idx] + [[0]] * (max_seq_len - len(trans[idx])) for idx in range(len(trans))]
    # print "pad_trans:{}".format(pad_trans)
    trans_length_list = [map(len, pdd_tran) for pdd_tran in pad_trans]
    # print "trans_length_list:{}".format(trans_length_list)
    max_tran_len = max(map(max, trans_length_list))
    # print "max_tran_len:{}".format(max_tran_len)
    trans_seq_tensor = autograd.Variable(torch.zeros((batch_size, max_seq_len, max_tran_len)),
                                         volatile=volatile_flag).long()
    trans_seq_lengths = torch.LongTensor(trans_length_list)
    for idx, (seq, seqlen) in enumerate(zip(pad_trans, trans_seq_lengths)):
        for idy, (tran, tranlen) in enumerate(zip(seq, seqlen)):
            trans_seq_tensor[idx, idy, :tranlen] = torch.LongTensor(tran)

    # print "trans_seq_tensor:{}".format(trans_seq_tensor)
    trans_seq_tensor = trans_seq_tensor[word_perm_idx].view(batch_size * max_seq_len, -1)
    # print "trans_seq_tensor:{}".format(trans_seq_tensor)
    trans_seq_lengths = trans_seq_lengths[word_perm_idx].view(batch_size * max_seq_len, )
    # print "trans_seq_lengths:{}".format(trans_seq_lengths)
    trans_seq_lengths, trans_perm_idx = trans_seq_lengths.sort(0, descending=True)
    trans_seq_tensor = trans_seq_tensor[trans_perm_idx]
    _, trans_seq_recover = trans_perm_idx.sort(0, descending=False)

    if gpu:
        word_seq_tensor = word_seq_tensor.cuda()
        for idx in range(feature_num):
            feature_seq_tensors[idx] = feature_seq_tensors[idx].cuda()
        word_seq_lengths = word_seq_lengths.cuda()
        word_seq_recover = word_seq_recover.cuda()
        label_seq_tensor = label_seq_tensor.cuda()
        char_seq_tensor = char_seq_tensor.cuda()
        char_seq_recover = char_seq_recover.cuda()
        trans_seq_tensor = trans_seq_tensor.cuda()
        trans_seq_recover = trans_seq_recover.cuda()
        mask = mask.cuda()
    return word_seq_tensor, feature_seq_tensors, word_seq_lengths, word_seq_recover, \
           char_seq_tensor, char_seq_lengths, char_seq_recover, \
           label_seq_tensor, \
           trans_seq_tensor, trans_seq_lengths, trans_seq_recover, mask


def evaluate(data, model, name, nbest=None):
    if name == "dev":
        instances = data.dev_Ids
    elif name == 'test':
        instances = data.test_Ids

    right_token = 0
    whole_token = 0
    nbest_pred_results = []
    pred_scores = []
    pred_results = []
    gold_results = []
    ## set model in eval model
    model.eval()
    batch_size = data.HP_batch_size
    start_time = time.time()
    train_num = len(instances)
    total_batch = train_num // batch_size + 1
    for batch_id in range(total_batch):
        start = batch_id * batch_size
        end = (batch_id + 1) * batch_size
        if end > train_num:
            end = train_num
        instance = instances[start:end]
        if not instance:
            continue
        batch_word, batch_features, batch_wordlen, batch_wordrecover, batch_char, batch_charlen, batch_charrecover, batch_label, batch_trans, trans_seq_lengths, trans_seq_recover, mask = batchify_with_label(
            instance, data.HP_gpu, True)
        if nbest:
            scores, nbest_tag_seq = model.decode_nbest(batch_word, batch_features, batch_wordlen, batch_char,
                                                       batch_charlen, batch_charrecover, mask, nbest, batch_trans,
                                                       trans_seq_lengths, trans_seq_recover)
            nbest_pred_result = recover_nbest_label(nbest_tag_seq, mask, data.label_alphabet, batch_wordrecover)
            nbest_pred_results += nbest_pred_result
            pred_scores += scores[batch_wordrecover].cpu().data.numpy().tolist()
            ## select the best sequence to evalurate
            tag_seq = nbest_tag_seq[:, :, 0]
        else:
            tag_seq = model(batch_word, batch_features, batch_wordlen, batch_char, batch_charlen, batch_charrecover,
                            mask, batch_trans, trans_seq_lengths, trans_seq_recover)
        # print "tag:",tag_seq
        pred_label, gold_label = recover_label(tag_seq, batch_label, mask, data.label_alphabet, batch_wordrecover)
        pred_results += pred_label
        gold_results += gold_label
    decode_time = time.time() - start_time
    speed = len(instances) / decode_time
    acc, p, r, f = get_ner_fmeasure(gold_results, pred_results, data.tagScheme)
    if nbest:
        return speed, acc, p, r, f, nbest_pred_results, pred_scores
    return speed, acc, p, r, f, pred_results, pred_scores


def recover_label(pred_variable, gold_variable, mask_variable, label_alphabet, word_recover):
    """
        input:
            pred_variable (batch_size, sent_len): pred tag result
            gold_variable (batch_size, sent_len): gold result variable
            mask_variable (batch_size, sent_len): mask variable
    """

    pred_variable = pred_variable[word_recover]
    # print pred_variable
    gold_variable = gold_variable[word_recover]
    mask_variable = mask_variable[word_recover]
    batch_size = gold_variable.size(0)
    seq_len = gold_variable.size(1)
    mask = mask_variable.cpu().data.numpy()
    pred_tag = pred_variable.cpu().data.numpy()
    gold_tag = gold_variable.cpu().data.numpy()
    batch_size = mask.shape[0]
    pred_label = []
    gold_label = []
    for idx in range(batch_size):
        # print "p:", pred_tag.tolist()
        # print "g:", gold_tag.tolist()
        pred = [label_alphabet.get_instance(pred_tag[idx][idy]) for idy in range(seq_len) if mask[idx][idy] != 0]
        gold = [label_alphabet.get_instance(gold_tag[idx][idy]) for idy in range(seq_len) if mask[idx][idy] != 0]
        # print "p:",pred, pred_tag.tolist()
        # print "g:", gold, gold_tag.tolist()
        assert (len(pred) == len(gold))
        pred_label.append(pred)
        gold_label.append(gold)
    return pred_label, gold_label


def recover_nbest_label(pred_variable, mask_variable, label_alphabet, word_recover):
    """
        input:
            pred_variable (batch_size, sent_len, nbest): pred tag result
            mask_variable (batch_size, sent_len): mask variable
            word_recover (batch_size)
        output:
            nbest_pred_label list: [batch_size, nbest, each_seq_len]
    """
    # print "word recover:", word_recover.size()
    # exit(0)
    pred_variable = pred_variable[word_recover]
    mask_variable = mask_variable[word_recover]
    batch_size = pred_variable.size(0)
    seq_len = pred_variable.size(1)
    # print pred_variable.size()
    nbest = pred_variable.size(2)
    mask = mask_variable.cpu().data.numpy()
    pred_tag = pred_variable.cpu().data.numpy()
    batch_size = mask.shape[0]
    pred_label = []
    for idx in range(batch_size):
        pred = []
        for idz in range(nbest):
            each_pred = [label_alphabet.get_instance(pred_tag[idx][idy][idz]) for idy in range(seq_len) if
                         mask[idx][idy] != 0]
            pred.append(each_pred)
        pred_label.append(pred)
    return pred_label


def predict_check(pred_variable, gold_variable, mask_variable):
    """
        input:
            pred_variable (batch_size, sent_len): pred tag result, in numpy format
            gold_variable (batch_size, sent_len): gold result variable
            mask_variable (batch_size, sent_len): mask variable
    """
    pred = pred_variable.cpu().data.numpy()
    gold = gold_variable.cpu().data.numpy()
    mask = mask_variable.cpu().data.numpy()
    overlaped = (pred == gold)
    right_token = np.sum(overlaped * mask)
    total_token = mask.sum()
    # print("right: %s, total: %s"%(right_token, total_token))
    return right_token, total_token


if __name__ == '__main__':
    a = np.arange(9.0)
    print a
    print norm2one(a)
