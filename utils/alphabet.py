# -*- coding: utf-8 -*-


"""
Alphabet maps objects to integer ids. It provides two way mapping from the index to the objects.
"""
import json
import os
from collections import OrderedDict


class Alphabet:
    def __init__(self, name, label=False, keep_growing=True):
        self.name = name  # Alphabet name
        self.UNKNOWN = "</unk>"
        self.label = label
        self.instance2index = OrderedDict()
        self.instances = []
        self.keep_growing = keep_growing

        # Index 0 is occupied by default, all else following. for padding
        self.default_index = 0
        self.next_index = 1  # 从1开始
        if not self.label:
            self.add(self.UNKNOWN)  # index = 1

    def clear(self, keep_growing=True):
        self.instance2index = {}
        self.instances = []
        self.keep_growing = keep_growing

        # Index 0 is occupied by default, all else following.
        self.default_index = 0
        self.next_index = 1

    def add(self, instance):
        """
        添加instance
        :param instance:
        :return:
        """
        if instance not in self.instance2index:
            self.instances.append(instance)
            self.instance2index[instance] = self.next_index
            self.next_index += 1

    def get_index(self, instance):
        """
        根据instance取id
        :param instance:
        :return:
        """
        try:
            return self.instance2index[instance]
        except KeyError:
            if self.keep_growing:
                index = self.next_index
                self.add(instance)
                return index
            else:
                print "{} not found,return unk!!!".format(instance)
                return self.instance2index[self.UNKNOWN]  # 如果没有找到，返回unk

    def get_instance(self, index):
        """
        根据id取instance
        :param index:
        :return:
        """
        if index == 0:
            # First index is occupied by the wildcard element.
            return None
        try:
            return self.instances[index - 1]
        except IndexError:
            print 'WARNING:{} Alphabet get_instance ,unknown instance:{}, return the first label.'.format(self.name,
                                                                                                          index)
            exit(0)
            return self.instances[0]  # 返回unk

    def size(self):
        # if self.label:
        #     return len(self.instances)
        # else:
        return len(self.instances) + 1  # 补了一个pad的位置

    def iteritems(self):
        return self.instance2index.iteritems()

    def enumerate_items(self, start=1):
        if start < 1 or start >= self.size():
            raise IndexError("Enumerate is allowed between [1 : size of the alphabet)")
        return zip(range(start, len(self.instances) + 1), self.instances[start - 1:])

    def close(self):
        self.keep_growing = False

    def open(self):
        self.keep_growing = True

    def get_content(self):
        return {'instance2index': self.instance2index, 'instances': self.instances}

    def from_json(self, data):
        self.instances = data["instances"]
        self.instance2index = data["instance2index"]

    def save(self, output_directory, name=None):
        """
        Save both alhpabet records to the given directory.
        :param output_directory: Directory to save model and weights.
        :param name: The alphabet saving name, optional.
        :return:
        """
        saving_name = name if name else self.__name
        try:
            json.dump(self.get_content(), open(os.path.join(output_directory, saving_name + ".json"), 'w'))
        except Exception as e:
            print("Exception: Alphabet is not saved: " % repr(e))

    def load(self, input_directory, name=None):
        """
        Load model architecture and weights from the give directory. This allow we use old models even the structure
        changes.
        :param input_directory: Directory to save model and weights
        :return:
        """
        loading_name = name if name else self.__name
        self.from_json(json.load(open(os.path.join(input_directory, loading_name + ".json"))))
