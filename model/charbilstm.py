# -*- coding: utf-8 -*-

import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import numpy as np


class CharBiLSTM(nn.Module):
    def __init__(self, alphabet_size, embedding_dim, hidden_dim, dropout, pretrain_char_embedding, gpu,
                 bidirect_flag=True):
        """

        :param alphabet_size: char词典大小
        :param embedding_dim: char embedding维度
        :param hidden_dim:
        :param dropout: dropout率
        :param pretrain_char_embedding:
        :param gpu:
        :param bidirect_flag:
        """
        super(CharBiLSTM, self).__init__()
        print "Build char sequence feature extractor: LSTM ..."
        self.gpu = gpu  # 是否使用GPU
        self.hidden_dim = hidden_dim  # 双向LSTM总维度
        if bidirect_flag:
            self.hidden_dim = hidden_dim // 2  # 返回小于除法运算结果的最大整数
        self.char_drop = nn.Dropout(dropout)
        self.char_embeddings = nn.Embedding(alphabet_size, embedding_dim)
        if pretrain_char_embedding is not None:
            self.char_embeddings.weight.data.copy_(torch.from_numpy(pretrain_char_embedding))
        else:
            self.char_embeddings.weight.data.copy_(
                torch.from_numpy(self.random_embedding(alphabet_size, embedding_dim)))

        self.char_lstm = nn.LSTM(embedding_dim, self.hidden_dim, num_layers=1, batch_first=True,
                                 bidirectional=bidirect_flag)
        if self.gpu:
            self.char_drop = self.char_drop.cuda()
            self.char_embeddings = self.char_embeddings.cuda()
            self.char_lstm = self.char_lstm.cuda()

    def random_embedding(self, vocab_size, embedding_dim):
        pretrain_emb = np.empty([vocab_size, embedding_dim])
        scale = np.sqrt(3.0 / embedding_dim)
        for index in range(vocab_size):
            pretrain_emb[index, :] = np.random.uniform(-scale, scale, [1, embedding_dim])
        return pretrain_emb

    def get_last_hiddens(self, input, seq_lengths):
        """
            input:  
                input: Variable(batch_size, word_length)
                seq_lengths: numpy array (batch_size,  1)
            output: 
                Variable(batch_size, char_hidden_dim)
            Note it only accepts ordered (length) variable, length size is recorded in seq_lengths
        """
        batch_size = input.size(0)
        # print "input:{}".format(input)
        # print "batch_size:{}".format(batch_size)
        char_embeds = self.char_drop(self.char_embeddings(input))  # input需要是二维的
        # print "char_embeds:{}".format(char_embeds)
        char_hidden = None
        # batch_first = True,input is expected in B x T x * format.
        # B:batch_size T:longest length
        pack_input = pack_padded_sequence(char_embeds, seq_lengths, True)
        # char_rnn_out:
        # char_hidden: (h_n, c_n)
        # h_n of shape (num_layers * num_directions, batch, hidden_size)-->2 x batch x hidden_size
        # batch_first = True
        char_rnn_out, char_hidden = self.char_lstm(pack_input, char_hidden)
        # print "char_hidden：{}".format(char_hidden)
        # batch_first = False return:T x B x *
        char_rnn_out, char_rnn_length = pad_packed_sequence(char_rnn_out)
        # print "char_rnn_out:{}".format(char_rnn_out)
        # print "char_hidden[0]:{}".format(char_hidden[0])
        # print "char_hidden[0].transpose(1, 0):{}".format(char_hidden[0].transpose(1, 0))
        # print "char_rnn_length:{}".format(char_rnn_length)

        # batch_size x 2 * hidden_dim，对应的长度
        return char_hidden[0].transpose(1, 0).contiguous().view(batch_size, -1), char_rnn_length

    def get_all_hiddens(self, input, seq_lengths):
        """
            input:  
                input: Variable(batch_size,  word_length)
                seq_lengths: numpy array (batch_size,  1)
            output: 
                Variable(batch_size, word_length, char_hidden_dim)
            Note it only accepts ordered (length) variable, length size is recorded in seq_lengths
        """
        batch_size = input.size(0)
        char_embeds = self.char_drop(self.char_embeddings(input))
        char_hidden = None
        pack_input = pack_padded_sequence(char_embeds, seq_lengths, True)
        char_rnn_out, char_hidden = self.char_lstm(pack_input, char_hidden)
        # output of shape (seq_len, batch, num_directions * hidden_size):
        char_rnn_out, _ = pad_packed_sequence(char_rnn_out)
        return char_rnn_out.transpose(1, 0)  # batch first (batch, seq_len, num_directions * hidden_size)

    def forward(self, input, seq_lengths):
        return self.get_all_hiddens(input, seq_lengths)
