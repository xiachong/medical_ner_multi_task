# -*- coding: utf-8 -*-


import argparse
import random
import traceback
from collections import OrderedDict

import torch

import numpy as np

from model.seqmodel import SeqModel
from model.wordsequence import WordSequence
from task.crtask import CrTask
from task.multitask import MultiTask
from task.nertask import NerTask
from utils.data import Data

seed_num = 42
random.seed(seed_num)
torch.manual_seed(seed_num)
np.random.seed(seed_num)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Muliti-task Named Entity recognition')
    parser.add_argument('--status', choices=['train', 'decode'], help='update algorithm', default='train')
    parser.add_argument('--max_step', default=1000, type=int, help='max_step')
    parser.add_argument('--eval_freq', default=1, type=int, help='eval_freq')
    parser.add_argument('--gpu', default=0, type=int, help='gpu')
    parser.add_argument('--task1_config', default="./task1.config", help='Task1 Configuration File')
    parser.add_argument('--task2_config', default="./task2.config", help='Task2 Configuration File')

    args = parser.parse_args()

    status = args.status.lower()  # train or test
    max_step = args.max_step  # 步数
    eval_freq = args.eval_freq  # eval频率

    torch.cuda.set_device(args.gpu)

    # task1
    print "********Build task1 data......********"
    task1_data = Data()
    task1_data.read_config(args.task1_config)
    task1_data.HP_gpu = torch.cuda.is_available() and task1_data.HP_gpu

    task1_data.build_alphabet(task1_data.train_dir)
    task1_data.build_alphabet(task1_data.dev_dir)
    task1_data.build_alphabet(task1_data.test_dir)
    if task1_data.use_trans:
        task1_data.build_translation_alphabet(task1_data.trans_dir)
        task1_data.build_translation_dict(task1_data.trans_dir)
    task1_data.fix_alphabet()

    task1_data.generate_instance('train')
    task1_data.generate_instance('dev')
    task1_data.generate_instance('test')
    task1_data.build_pretrain_emb()

    task1_data.show_data_summary()

    # task2
    print "********Build task2 data......********"
    task2_data = Data()
    task2_data.read_config(args.task2_config)
    task2_data.HP_gpu = torch.cuda.is_available() and task2_data.HP_gpu

    task2_data.build_alphabet(task2_data.train_dir)
    task2_data.build_alphabet(task2_data.dev_dir)
    task2_data.build_alphabet(task2_data.test_dir)
    if task2_data.use_trans:
        task2_data.build_translation_alphabet(task2_data.trans_dir)
        task2_data.build_translation_dict(task2_data.trans_dir)
    task2_data.fix_alphabet()

    task2_data.generate_instance('train')
    task2_data.generate_instance('dev')
    task2_data.generate_instance('test')
    task2_data.build_pretrain_emb()

    # print "task2 label:{}".format(task2_data.label_alphabet.instance2index)
    # print "task2 text:{}".format(task2_data.dev_texts)
    # print "task2 ids:{}".format(task2_data.dev_Ids)
    # exit(0)

    task2_data.show_data_summary()

    if status == 'train':
        print("MODEL: train")

        # cross-task word-sequence
        # use either task1_data or task2_data is ok
        word_sequence = WordSequence(task1_data, "cross-task WordSequence")

        # make sure whether the word representation is the same one.
        task1_model = SeqModel(task1_data, word_sequence)
        print "task1_model cross-task Word Sequence:{}".format(task1_model.word_hidden.name)
        task2_model = SeqModel(task2_data, word_sequence)
        print "task2_model cross-task Word Sequence:{}".format(task2_model.word_hidden.name)

        if task1_data.HP_gpu and task2_data.HP_gpu and torch.cuda.is_available():
            word_sequence.cuda()
            task1_model.cuda()
            task2_model.cuda()

        print "**********************************"
        print "task1_model:{}".format(task1_model)
        print "task2_model:{}".format(task2_model)
        print "**********************************"

        # exit()

        ner_task = NerTask("ner", task1_model, task1_data, ref=True)
        cr_task = CrTask("cr", task2_model, task2_data, aux_task=True)
        tasks = [ner_task, cr_task]

        multitask = MultiTask(tasks, eval_freq=eval_freq)

        try:
            for step in range(1, max_step + 1):
                # print "step:{}".format(step)
                multitask.step()
        except Exception:
            traceback.print_exc()
