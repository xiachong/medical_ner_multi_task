from sequencetask import SequenceTask
from utils.functions import evaluate


class CrTask(SequenceTask):
    def eval(self, dataset_name, log_output=None):
        """
        evaluate
        :param dataset_name:
        :param log_output:
        :return:
        """
        speed, acc, p, r, f, _, _ = evaluate(self.data, self.model, dataset_name)
        return f
