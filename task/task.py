# coding=utf-8

import torch.optim as optim


class Task(object):
    """
    Task基类，泛化代表某一种任务
    """

    def __init__(self, name, model, data, prob=1.0, require_eval=True, ref=False, aux_task=False, aux_lang=False):
        self.name = name  # 任务名称
        self.model = model  # 任务对应的模型
        self.prob = prob  # 该任务被选取的概率
        self.require_eval = require_eval  # 是否需要测试

        self.task_step = 0  # 任务执行步数
        self.decay_step = data.HP_decay_step

        self.data = data

        self.aux_task = aux_task  # 是否是辅助任务
        self.aux_lang = aux_lang

        self.ref = ref

        self.train_batch_id = 0
        self.dev_batch_id = 0
        self.test_batch_id = 0
        self.have_trained = False

        if self.data.optimizer.lower() == "sgd":
            self.optimizer = optim.SGD(model.parameters(), lr=data.HP_lr, momentum=data.HP_momentum,
                                       weight_decay=data.HP_l2)
            if self.data.use_mapping:
                self.optimizer_wc = optim.SGD(model.word_hidden.wordrep.w.parameters(), lr=data.HP_lr,
                                              momentum=data.HP_momentum,
                                              weight_decay=data.HP_l2)
        elif self.data.optimizer.lower() == "adam":
            self.optimizer = optim.Adam(model.parameters(), lr=data.HP_lr, weight_decay=data.HP_l2)
            if self.data.use_mapping:
                self.optimizer_wc = optim.Adam(model.word_hidden.wordrep.w.parameters(), lr=data.HP_lr,
                                               weight_decay=data.HP_l2)
        else:
            print("Optimizer illegal: %s , use sgd or adam." % data.optimizer)
            exit(0)

        if self.data.HP_gpu:
            self.model.cuda()

        # self.train = data.get('train', None)  # 训练集
        # self.dev = data.get('dev', None)  # 开发集
        # self.test = data.get('test', None)  # 测试集
        # self.vocabs = vocabs
        # self.token_vocab = vocabs.get('token')  # 词语词典
        # self.label_vocab = vocabs.get('label')  # label词典
        # self.char_vocab = vocabs.get('char')  # 字符词典

        # self.lr = lr  # 学习率
        # self.momentum = momentum  # 动量
        # self.decay_rate = decay_rate  # 学习率衰减
        # self.gradient_clipping = gradient_clipping  # 梯度裁剪
        #
        # self.aux_lang = aux_lang  # 辅助语言

        # self.optimizer = optim.SGD(
        #     filter(lambda p: p.requires_grad, model.parameters()),
        #     lr=lr, momentum=momentum)  # 优化器

    def step(self):
        raise NotImplementedError()

    def eval(self, dataset_name, log_output=None):
        raise NotImplementedError()

    def learning_rate_decay(self):
        """
        学习率衰减
        :return:
        """
        lr = self.data.HP_lr * self.data.HP_lr_decay ** (self.task_step / self.decay_step)
        # print "learning rate is setted as:{}".format(lr)
        for p in self.optimizer.param_groups:
            p['lr'] = lr
