# coding=utf-8
import torch
from numpy.random import choice


class MultiTask(object):
    """
    多任务类
    """

    def __init__(self, tasks, eval_freq=1000):
        print "*******************************"
        print "Init multi-task ......"
        self.tasks = tasks  # 多个任务 list
        self.task_probs = []  # 每个任务的选取概率
        self.update_probs()  # 初始化任务选取概率
        self.global_step = 0  # 全局步数
        self.eval_freq = eval_freq  # 多少步evaluate一次

        # 哪一个作为基准任务
        self.ref_task = 0
        self.best_ref_score = -1.0
        self.best_scores = []

        for task_idx, task in enumerate(self.tasks):
            if task.ref:
                print "ref task name:{}".format(task.name)
                print "ref task id:{}".format(task_idx)
                self.ref_tasks = task_idx
                break

        # show multi-task init summary
        print "tasks Info..."
        for task_id, task in enumerate(self.tasks):
            print "task_id:{},task name:{}".format(task_id, task.name)
        print "init task_probs:{}".format(self.task_probs)

    def update_probs(self):

        def auto_prob(task):
            doc_num = len(task.data.train_Ids)  # number of training examples
            theta_task = .1 if task.aux_task else 1
            theta_lang = .1 if task.aux_lang else 1
            prob = doc_num ** .5 * theta_task * theta_lang
            return prob

        task_probs = [auto_prob(t) for t in self.tasks]
        task_prob_sum = sum(task_probs)
        self.task_probs = [p / task_prob_sum for p in task_probs]

    def step(self):
        self.global_step += 1
        # 选择一个任务
        task = choice(self.tasks, p=self.task_probs)
        task.have_trained = True
        print "step:{}  task---->{}".format(self.global_step, task.name)
        # 更新该任务的学习率
        task.learning_rate_decay()
        # 执行该任务
        task.step()

        if self.global_step % self.eval_freq == 0:
            scores = []
            ref_score = 0
            # 对于每一个任务
            for task_idx, task in enumerate(self.tasks):
                if task.require_eval and task.have_trained:
                    dev_scores = task.eval('dev')  # eval开发集
                    test_scores = task.eval('test')  # eval测试集
                    if task_idx == self.ref_task:
                        ref_score = dev_scores  # 哪一个score作为标准
                    print "task name:{}======>dev_f_scores:{},test_f_scores:{}".format(task.name, dev_scores,
                                                                                       test_scores)
                    scores.append((task_idx, dev_scores, test_scores))
            if ref_score > self.best_ref_score:
                self.best_ref_score = ref_score
                # self.best_scores = scores
                # if task.data.seg:
                print "Exceed previous best f score:", self.best_ref_score
                # model_name = task.data.model_dir + '.' + str(self.global_step) + ".model"
                # print "Save current best model in file:", model_name
                # torch.save(task.model.state_dict(), model_name)
                # best_dev = current_score
