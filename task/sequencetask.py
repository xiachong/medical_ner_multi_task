# coding=utf-8
import random
import sys

import time

from task import Task
from utils.functions import get_instance, batchify_with_label, predict_check


class SequenceTask(Task):
    """
    序列标注任务
    """

    def __init__(self, name, model, data, prob=1.0, require_eval=True, ref=False, aux_task=False):
        super(SequenceTask, self).__init__(name, model, data, prob, require_eval, ref, aux_task)

    def step(self):
        """
        一个batch的前向传播
        :return: 无
        """

        epoch_start = time.time()
        temp_start = epoch_start

        instance_count = 0
        sample_id = 0

        sample_loss = 0
        sample_mapping_loss = 0

        total_loss = 0
        total_mapping_loss = 0

        right_token = 0
        whole_token = 0

        random.shuffle(self.data.train_Ids)

        self.task_step += 1  # 步数 + 1
        self.model.train()  # 训练状态
        self.model.zero_grad()  # 梯度清零
        batch_size = self.data.HP_batch_size
        batch_id = 0
        train_num = len(self.data.train_Ids)
        total_batch = train_num // batch_size + 1

        for batch_id in range(total_batch):
            start = batch_id * batch_size
            end = (batch_id + 1) * batch_size
            if end > train_num:
                end = train_num
            instance = self.data.train_Ids[start:end]

            if not instance:
                continue
            batch_word, batch_features, batch_wordlen, batch_wordrecover, batch_char, batch_charlen, batch_charrecover, batch_label, batch_trans, trans_seq_lengths, trans_seq_recover, mask = batchify_with_label(
                instance, self.data.HP_gpu)
            instance_count += 1

            loss, tag_seq, wc_loss = self.model.neg_log_likelihood_loss(batch_word, batch_features, batch_wordlen,
                                                                        batch_char,
                                                                        batch_charlen, batch_charrecover, batch_label,
                                                                        mask,
                                                                        batch_trans, trans_seq_lengths,
                                                                        trans_seq_recover)
            right, whole = predict_check(tag_seq, batch_label, mask)
            right_token += right
            whole_token += whole
            sample_loss += loss.data[0]
            if self.data.use_mapping:
                sample_mapping_loss += wc_loss.data[0]

            total_loss += loss.data[0]
            if self.data.use_mapping:
                total_mapping_loss += wc_loss.data[0]
            if end % 500 == 0:
                temp_time = time.time()
                temp_cost = temp_time - temp_start
                temp_start = temp_time
                if self.data.use_mapping:
                    print("     Instance: %s; Time: %.2fs; loss: %.4f; acc: %s/%s=%.4f" % (
                        end, temp_cost, sample_loss, right_token, whole_token, (right_token + 0.) / whole_token))
                else:
                    print("     Instance: %s; Time: %.2fs; loss: %.4f;mapping_loss: %.4f; acc: %s/%s=%.4f" % (
                        end, temp_cost, sample_loss, sample_mapping_loss, right_token, whole_token,
                        (right_token + 0.) / whole_token))
                sys.stdout.flush()
                sample_loss = 0
                sample_mapping_loss = 0

            if self.data.use_trans and self.data.use_mapping:
                for param in self.model.word_hidden.wordrep.w.parameters():
                    param.requires_grad = False
                loss.backward(retain_graph=True)
                # torch.nn.utils.clip_grad_norm(model.parameters(), data.HP_clip)
                self.optimizer.step()
                self.model.zero_grad()
                for param in self.model.word_hidden.wordrep.w.parameters():
                    param.requires_grad = True
                wc_loss.backward()
                self.optimizer_wc.step()
                self.model.zero_grad()
            else:
                loss.backward()
                # torch.nn.utils.clip_grad_norm(model.parameters(), data.HP_clip)
                self.optimizer.step()
                self.model.zero_grad()

        temp_time = time.time()
        temp_cost = temp_time - temp_start
        if self.data.use_mapping:
            print("     Instance: %s; Time: %.2fs; loss: %.4f; acc: %s/%s=%.4f" % (
                end, temp_cost, sample_loss, right_token, whole_token, (right_token + 0.) / whole_token))
        else:
            print("     Instance: %s; Time: %.2fs; loss: %.4f;mapping_loss: %.4f; acc: %s/%s=%.4f" % (
                end, temp_cost, sample_loss, sample_mapping_loss, right_token, whole_token,
                (right_token + 0.) / whole_token))
        epoch_finish = time.time()
        epoch_cost = epoch_finish - epoch_start
        if self.data.use_mapping:
            print(
                    "Step: %s training finished. Time: %.2fs, speed: %.2fst/s,  total loss: %s,total mapping loss: %s" % (
                self.task_step, epoch_cost, train_num / epoch_cost, total_loss, total_mapping_loss))
        else:
            print("Step: %s training finished. Time: %.2fs, speed: %.2fst/s,  total loss: %s" % (
                self.task_step, epoch_cost, train_num / epoch_cost, total_loss))

        #
        # sample_loss = 0
        # sample_mapping_loss = 0  # 如果使用mapping的话 有mapping loss
        #
        # right_token = 0
        # whole_token = 0
        #
        # self.task_step += 1  # 步数 + 1
        # self.model.train()  # 训练状态
        # self.model.zero_grad()  # 梯度清零
        # # 获取一个batch的数据:instance
        # instance, self.train_batch_id = get_instance(self.train_batch_id, self.data, "train")
        # # padding
        # batch_word, batch_features, batch_wordlen, batch_wordrecover, batch_char, batch_charlen, batch_charrecover, batch_label, batch_trans, trans_seq_lengths, trans_seq_recover, mask = batchify_with_label(
        #     instance, self.data.HP_gpu)
        # loss, tag_seq, wc_loss = self.model.neg_log_likelihood_loss(batch_word, batch_features, batch_wordlen,
        #                                                             batch_char,
        #                                                             batch_charlen, batch_charrecover, batch_label, mask,
        #                                                             batch_trans, trans_seq_lengths, trans_seq_recover)
        # right, whole = predict_check(tag_seq, batch_label, mask)
        # right_token += right
        # whole_token += whole
        # sample_loss += loss.data[0]
        # if self.data.use_mapping:
        #     sample_mapping_loss += wc_loss.data[0]
        #
        # if self.data.use_mapping:
        #     print("loss: %.4f; acc: %s/%s=%.4f" % (
        #         sample_loss, right_token, whole_token, (right_token + 0.) / whole_token))
        # else:
        #     print("loss: %.4f;mapping_loss: %.4f; acc: %s/%s=%.4f" % (
        #         sample_loss, sample_mapping_loss, right_token, whole_token,
        #         (right_token + 0.) / whole_token))
        # sys.stdout.flush()
        #
        # if self.data.use_trans and self.data.use_mapping:
        #     for param in self.model.word_hidden.wordrep.w.parameters():
        #         param.requires_grad = False
        #     loss.backward(retain_graph=True)
        #     # torch.nn.utils.clip_grad_norm(model.parameters(), data.HP_clip)
        #     self.optimizer.step()
        #     self.model.zero_grad()
        #     for param in self.model.word_hidden.wordrep.w.parameters():
        #         param.requires_grad = True
        #     wc_loss.backward()
        #     self.optimizer_wc.step()
        #     self.model.zero_grad()
        # else:
        #     loss.backward()
        #     # torch.nn.utils.clip_grad_norm(model.parameters(), data.HP_clip)
        #     self.optimizer.step()
        #     self.model.zero_grad()

        # self.optimizer.zero_grad()  # 梯度清零
        # 获取batch数据
        # (tokens, labels, chars, seq_lens, char_lens) = self.train.get_batch(gpu=self.gpu)
        # # 前向传播
        # loglik, _ = self.model.loglik(tokens, labels, seq_lens, chars,
        #                               char_lens)
        # loss = -loglik.mean()
        # # 反向传播
        # loss.backward()
        #
        # params = []
        # for n, p in self.model.named_parameters():
        #     if 'embedding.weight' not in n:
        #         params.append(p)
        # # 梯度裁剪
        # clip_grad_norm(params, self.gradient_clipping)
        # # 梯度更新
        # self.optimizer.step()
